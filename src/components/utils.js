export const colors = {
    // std colors
    RED: 'red',
    BLACK: 'black',
    GREY: 'grey',
    CONTAINER_MAIN: '#ecf0f1',
    INPUT_BACKGROUND: '#E6F6FF',
    BUTTON_BG: '#FBFFF3',
};
