export const buttons = [
    {
        type: 'control',
        val: ['=', 'AC']
    },
    {
        type: 'operation',
        val: ['+', '-', '*', '/']
    },
    {
        type: 'number',
        val: [[1, 2, 3, 4], [5, 6, 7, 8], [9, 0, 'A', 'B'], ['C', 'D', 'E', 'F']]
    }
];
