import { StyleSheet } from 'react-native';
import { colors } from './utils';

export const styles = StyleSheet.create({
    textStyle: {
        fontSize: 16,
        fontWeight: 'bold',
        lineHeight: 25,
    },
    inputStyle: {
        backgroundColor: colors.INPUT_BACKGROUND,
        fontSize: 16,
    },
});
